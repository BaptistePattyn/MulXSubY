/**
 * class that implements the mulXsubY method 
 */

package MulXSubY;

import java.util.ArrayList;
import java.util.List;

public class MulXSubY {
	private int mul, sub, start, end;
	private LinkedBinaryTree<Integer> tree;
	private boolean found = false;
	/*
	 * consturctor for a mulxsuby with given value for the multiplication, the substraction, the start and the end
	 */
	public MulXSubY(int mul, int sub, int start, int end) {
		tree = new LinkedBinaryTree<>();
		this.start = start;
		this.end = end;
		this.mul = mul;
		this.sub = sub;
		tree.addRoot(start);
	}
	/*
	 * adds a layer to the tree, only if the end value is not reached
	 */
	public void addLayer() {
		System.out.println(tree.size());
		for(Position<Integer> p : tree.positions()) {
			//System.out.println(p.getElement());
			if(tree.isExternal(p)) {
				if((p.getElement()*mul == end)) {
					tree.addLeft(p, p.getElement()*mul);
					printResult(tree.left(p));
					found = true;
					break;
				}else if((p.getElement()-sub == end)) {
					tree.addRight(p, p.getElement()-sub);
					printResult(tree.right(p));
					found = true;
					break;
				}else{
				tree.addLeft(p, p.getElement()*mul);
				tree.addRight(p, p.getElement()-sub);
				}
			}
		}
	}
	/*
	 * prints the result of the calculations made to get from the start value to the end value
	 */
	public void printResult(Position<Integer> p) {
		StringBuilder sb = new StringBuilder();
		Position<Integer> parent = tree.parent(p);
		do {			
			if(p.equals(tree.left(parent))) {
				sb.insert(0, parent.getElement() + "*" + mul + " = " + p.getElement() +"\n" );
			} else {
				sb.insert(0, parent.getElement() + "-" + sub + " = " + p.getElement() +"\n");
			}
			p = parent;
			parent = tree.parent(parent);
		} while(!(tree.parent(p)== null));
		sb.toString();
		System.out.println(sb);
	}
	/*
	 * execute the calculations
	 */
	public void calculate(){
		while(!found) {			
			addLayer();
		}
	}
		
	public LinkedBinaryTree<Integer> getTree(){
		return tree;
	}
	
}
