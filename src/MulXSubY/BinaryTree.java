package MulXSubY;

public interface BinaryTree<E> extends Tree<E> {
	/*returns the left position of a position */
	Position<E> left(Position<E> p) throws IllegalArgumentException;
	/*returns the right position of a position */
	Position<E> right(Position<E> p) throws IllegalArgumentException;
	/*returns the sibling position of a position */
	Position<E> sibling(Position<E> p) throws IllegalArgumentException;
}
