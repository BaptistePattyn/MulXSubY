package MulXSubY;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AbstractTree<E> implements Tree<E>,Iterable<E> {

	@Override
	public Position<E> root() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Position<E> parent(Position<E> p) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Iterable<Position<E>> children(Position<E> p) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int numChildren(Position<E> p) throws IllegalArgumentException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isInternal(Position<E> p) throws IllegalArgumentException {
		return numChildren(p) > 0;
	}

	@Override
	public boolean isExternal(Position<E> p) throws IllegalArgumentException {
		return numChildren(p) == 0;
	}

	@Override
	public boolean isRoot(Position<E> p) throws IllegalArgumentException {
		return p == root();
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public boolean isEmpty() {
		return size() == 0;
	}
	/*Returns an iterator of the elements stored in the tree */
	@Override
	public Iterator<E> iterator() {
		return new ElementIterator();
	}
	/*This class adapts the iteration produced by positions() to return elements */
	private class ElementIterator implements Iterator<E>{
		Iterator<Position<E>> posIterator = positions().iterator();
		@Override
		public boolean hasNext() {
			return posIterator.hasNext();
		}

		@Override
		public E next() {
			return posIterator.next().getElement();
		}
		
		public void remove() {posIterator.remove();}
		
	}

	@Override
	public Iterable<Position<E>> positions() {
		return preorder();
	}
	/* Adds positions of the subtree rooted at Position p to the given snapshot*/
	private void preorderSubtree(Position<E> p, List<Position<E>> snapshot) {
		snapshot.add(p);
		for(Position<E> c : children(p))
			preorderSubtree(c, snapshot);
	}
	/* runs an iterable collection of positions of the tree, reported in preorder */
	public Iterable<Position<E>> preorder(){
		List<Position<E>> snapshot = new ArrayList<>();
		if(!isEmpty()) {
			preorderSubtree(root(),snapshot);
		}
		return snapshot;
	}

}
