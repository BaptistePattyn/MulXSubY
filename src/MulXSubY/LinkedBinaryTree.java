package MulXSubY;

import java.util.Iterator;

public class LinkedBinaryTree<E> extends AbstractBinaryTree<E> implements Iterable<E>{

	
	protected static class Node<E> implements Position<E>{
		private E element;
		private Node<E> parent, left, right;
		
		/*Constructs a node with the given element and neighbors */
		public Node(E e, Node<E> above, Node<E> leftChild, Node<E> rightChild) {
			element = e;
			parent = above;
			left = leftChild;
			right = rightChild;
		}
		/*Returns the element from the node */
		@Override
		public E getElement() {
			return element;
		}
		/*Checks if a node has children */
		public boolean hasChild() {
			return (getLeft() != null) || (getRight() != null);
		}
		/*Returns the parent from the node */
		public Node<E> getParent(){
			return parent;
		}
		/*Returns the left child from the node */
		public Node<E> getLeft(){
			return left;
		}
		/*Returns the right child from the node */
		public Node<E> getRight(){
			return right;
		}
		/*Sets the element from the node */
		public void setElement(E e) {
			element = e;
		}
		/*Sets the parent from the node */
		public void setParent(Node<E> parentNode) {
			parent = parentNode;
		}
		/*Sets the left child from the node */
		public void setLeft(Node<E> leftChild) {
			left = leftChild;
		}
		/*Sets the right child from the node */
		public void setRight(Node<E> rightChild) {
			right = rightChild;
		}
	}
	
	protected Node<E> createNode(E e, Node<E> parent, Node<E> left, Node<E> right){
		return new Node<E>(e, parent, left, right);
	}
	
	protected Node<E> root = null;
	private int size = 0;
	
	public LinkedBinaryTree() {}
	/* validates the position and returns it as a node */
	protected Node<E> validate(Position<E> p) throws IllegalArgumentException{
		if(!(p instanceof Node))
			throw new IllegalArgumentException("Not valid position type");
		Node<E> node = (Node<E>) p;
		if(node.getParent() == node)
			throw new IllegalArgumentException("p is no longer in the tree");
		
		return node;
	}
	/* returns the number of nodes in the tree */
	@Override
	public int size() {
		return size;
	}
	/* returns the root of the tree */
	public Position<E> root(){
		return root;
	}
	/* returns the parent of the position */
	public Position<E> parent(Position<E> p) throws IllegalArgumentException{
		Node<E> node = validate(p);
		return node.getParent();
	}
	/* returns the left child of the position */
	public Position<E> left(Position<E> p) throws IllegalArgumentException{
		Node<E> node = validate(p);
		return node.getLeft();
	}
	/* returns the right child of the position */
	public Position<E> right(Position<E> p) throws IllegalArgumentException{
		Node<E> node = validate(p);
		return node.getRight();
	}
	/* adds the root to the tree*/
	public Position<E> addRoot(E e) throws IllegalStateException{
		if(!isEmpty()) throw new IllegalStateException("Tree is not empty");
		root = createNode(e, null,null, null);
		size = 1;
		return root;
 	}
	/* adds a left position to the position*/
	public Position<E> addLeft(Position<E> p, E e) throws IllegalArgumentException{
		Node<E> parent = validate(p);
		if(parent.getLeft() != null)
			throw new IllegalArgumentException("p already has a left child");
		Node<E> child = createNode(e, parent, null, null);
		parent.setLeft(child);
		size++;
		return child;
	}
	/* adds a right position to the position*/
	public Position<E> addRight(Position<E> p, E e) throws IllegalArgumentException{
		Node<E> parent = validate(p);
		if(parent.getRight() != null)
			throw new IllegalArgumentException("p already has a right child");
		Node<E> child = createNode(e, parent, null, null);
		parent.setRight(child);
		size++;
		return child;
	}
	/* replaces the element of the position */
	public E set(Position<E> p , E e) throws IllegalArgumentException{
		Node<E> node = validate(p);
		E temp = node.getElement();
		node.setElement(temp);
		return temp;
	}
	/* attaches trees t1 and t2 as left and right subtrees to the position */
	public void attach(Position<E> p, LinkedBinaryTree<E> t1, LinkedBinaryTree<E> t2) throws IllegalArgumentException{
		Node<E> node = validate(p);
		if(isInternal(p)) throw new IllegalArgumentException("p must be a leaf");
		size+= t1.size() + t2.size();
		if(!t1.isEmpty()) {
			t1.root.setParent(node);
			node.setLeft(t1.root);
			t1.root = null;
			t1.size = 0;
		}
		
		if(!t2.isEmpty()) {
			t2.root.setParent(node);
			node.setLeft(t2.root);
			t2.root = null;
			t2.size = 0;
		}		
	}
	/* removes the node at position p and replaces it with its child if it has any */
	public E remove(Position<E> p) throws IllegalArgumentException{
		Node<E> node = validate(p);
		if(numChildren(p) == 2)
			throw new IllegalArgumentException("p has two children");
		Node<E> child = (node.getLeft() != null ? node.getLeft() : node.getRight());
		if(child != null)
			child.setParent(node.getParent());
		if(node == root)
			root = child;
		else {
			Node<E> parent = node.getParent();
			if(node == parent.getLeft())
				parent.setLeft(child);
			else
				parent.setRight(child);
		}
		size--;
		E temp = node.getElement();
		node.setElement(null);
		node.setLeft(null);
		node.setRight(null);
		node.setParent(node);
		return temp;
	}
	
}
